package com.tast2;

public class Tv implements TVremote {

	@Override
	public void tvremote() {
		System.out.println("TV Remote");
		
	}

	@Override
	public void smarttvremote() {
		System.out.println("Smart TV Remote");
		
	}
	
	public static void main(String[] args) {
		Tv t=new Tv();
		t.tvremote();
		t.smarttvremote();
	}

}
