package com.tast2;

public class Area extends Shape {

	@Override
	public void rectangle_area(int a, int b) {
		 System.out.println("the area of the rectangle is "+a*b+" sq units");
		
	}

	@Override
	public void square_area(int c) {
		
		System.out.println("the area of the square is "+Math.pow(c, 2)+" sq units");
		
	}

	@Override
	public void circle_area(int r) {
		 double z = 3.14 * r * r;
	        System.out.println("the area of the circle is "+z+" sq units");
		
	}
public static void main(String[] args) {
	
	Area a1=new Area();
	a1.rectangle_area(10, 20);
	a1.square_area(4);
	a1.circle_area(10);
}
}
