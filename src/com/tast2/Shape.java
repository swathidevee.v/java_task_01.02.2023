package com.tast2;

public abstract class Shape {
public abstract void rectangle_area(int a, int b);
public abstract void square_area(int c);
public abstract void circle_area(int r);
}
