package com.task;

public abstract class Book {

	public abstract void write();
	public abstract void read();
	
}
